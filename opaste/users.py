from flask import Blueprint, render_template, abort

from opaste.db import getDb

bp = Blueprint('users', __name__)

@bp.route('/u/<username>')
def view(username):
    db = getDb()
    user = db.users.find_one({'name': username})

    if user is None:
        abort(404)

    pastes = db.pastes.find({'user': user['_id']})

    return render_template('users/view.html', user=user, pastes=pastes)