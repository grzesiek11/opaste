import string, random, datetime

from flask import Blueprint, request, session, flash, redirect, url_for, render_template, Markup, escape, g, abort

from bson import ObjectId

import werkzeug.exceptions as exceptions

from opaste.auth import loginRequired
from opaste.db import getDb

bp = Blueprint('pastes', __name__)

def getPaste(id):
    return getDb().pastes.find_one({'_id': id})

@bp.route('/new', methods=('GET', 'POST'))
def new():
    if request.method == 'POST':
        content = request.form['content']
        db = getDb()
        error = None

        if not content:
            error = 'Content is required'

        if error is None:
            id = ''.join(random.choices(string.ascii_letters + string.digits, k=10))
            timestamp = int(datetime.datetime.now().timestamp())
            db.pastes.insert_one({
                '_id': id,
                'content': content,
                'user': g.user['_id'] if g.user is not None else None,
                'creationTimestamp': timestamp,
                'modifyTimestamp': timestamp
            })
            return redirect(url_for('pastes.view', id=id))
        else:
            flash(error)

    return render_template('pastes/new.html')

@bp.route('/p/<id>')
def view(id):
    return render_template('pastes/view.html', paste=getDb().pastes.find_one({'_id': id}))

@bp.route('/p/<id>/raw')
def raw(id):
    return getPaste(id)['content']

@bp.route('/p/<id>/delete', methods=('POST',))
@loginRequired
def delete(id):
    paste = getPaste(id)

    if paste is None:
        abort(404)
    elif paste['user'] != g.user['_id']:
        abort(403)
    
    return redirect(url_for('index'))

@bp.route('/p/<id>/edit', methods=('GET', 'POST'))
@loginRequired
def edit(id):
    paste = getPaste(id)

    if paste is None:
        abort(404)
    elif paste['user'] != g.user['_id']:
        abort(403)
    
    if request.method == 'POST':
        pasteContent = request.form['pasteContent']
        db = getDb()
        error = None

        if not pasteContent:
            error = 'Content is required'

        if error is None:
            db.pastes.update_one({'_id': id}, {'$set': {'content': pasteContent}})
            return redirect(url_for('pastes.view', id=id))
        else:
            flash(error)
    
    return render_template('pastes/edit.html', paste=paste)